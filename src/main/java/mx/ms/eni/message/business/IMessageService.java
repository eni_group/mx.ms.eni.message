package mx.ms.eni.message.business;

import mx.ms.eni.message.model.dto.MessageDTO;

public interface IMessageService {

	MessageDTO getMessage(Integer idMessage);
	
	void newMessage(MessageDTO messageDTO);
	
	void deleteMessage(Integer idMessage);

}
