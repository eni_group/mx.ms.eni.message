package mx.ms.eni.message.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ms.eni.message.business.IMessageService;
import mx.ms.eni.message.model.dto.MessageDTO;
import mx.ms.eni.message.model.entity.MessageEntity;
import mx.ms.eni.message.model.repository.IMessageRepository;

@Service
public class MessageServiceImpl implements IMessageService{

	@Autowired
	IMessageRepository messageRepository = null; 
	
	@Autowired
	MessageDTO messageDTO = null;
	
	public MessageDTO getMessage(Integer idMessage)
	{
		MessageEntity messageEntity = messageRepository.findByIdMessage(idMessage);
		if(messageEntity != null)
		{
			messageDTO = new MessageDTO();
			messageDTO.setIdMessage(messageEntity.getIdMessage());
			messageDTO.setNamespace(messageEntity.getNamespace());
			messageDTO.setMessage(messageEntity.getMessage());
		}
		return messageDTO;
	}
	
	
	public void newMessage(MessageDTO messageDTO)
	{
		MessageEntity messageEntity = new MessageEntity();
		messageEntity.setIdMessage(messageDTO.getIdMessage());
		messageEntity.setNamespace(messageDTO.getNamespace());
		messageEntity.setMessage(messageDTO.getMessage());
		messageRepository.save(messageEntity);
	}
	
	public void deleteMessage(Integer idMessage)
	{
		messageRepository.deleteById(idMessage);
	}
}
