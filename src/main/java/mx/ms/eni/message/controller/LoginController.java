package mx.ms.eni.message.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.ms.eni.message.business.IMessageService;
import mx.ms.eni.message.model.dto.MessageDTO;
import mx.ms.eni.message.service.IMessagePublicService;

@RestController
@RequestMapping(value="/message")
public class LoginController implements IMessagePublicService{

	@Autowired
	IMessageService messageService = null;
	
	@Override
	@CrossOrigin(origins="*")
	@RequestMapping(value="/", produces="application/json", method = {RequestMethod.GET})
	public MessageDTO getMessage(@RequestParam Integer idMessage)
	{
		return messageService.getMessage(idMessage);
	}
	
	@Override
	@CrossOrigin(origins="*")
	@PostMapping(value="/", consumes = "application/json", produces="application/json")
	public void newMessage(@RequestBody MessageDTO messageDTO)
	{
		messageService.newMessage(messageDTO);
	}
	
	@Override
	@CrossOrigin(origins="*")
	@DeleteMapping(value="/", consumes = "application/json", produces="application/json")
	public void deleteMessage(@RequestBody Integer idMessage)
	{
		messageService.deleteMessage(idMessage);
	}
}
