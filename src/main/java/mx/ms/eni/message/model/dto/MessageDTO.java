package mx.ms.eni.message.model.dto;

import javax.persistence.Column;

public class MessageDTO {

private Integer idMessage;
	
	@Column(name="Namespace", nullable = false)
	private Integer namespace;
	
	@Column(name="Mensaje", nullable = false)
	private String message;
	
	public MessageDTO() { 
		
	}

	public Integer getIdMessage() {
		return idMessage;
	}

	public void setIdMessage(Integer idMessage) {
		this.idMessage = idMessage;
	}

	public Integer getNamespace() {
		return namespace;
	}

	public void setNamespace(Integer namespace) {
		this.namespace = namespace;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MessageDTO [idMessage=" + idMessage + ", namespace=" + namespace + ", message=" + message + "]";
	}
	
}
