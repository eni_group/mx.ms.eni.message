package mx.ms.eni.message.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Mensaje")
public class MessageEntity implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idMensaje")
	private Integer idMessage;
	
	@Column(name="Namespace", nullable = false)
	private Integer namespace;
	
	@Column(name="Mensaje", nullable = false)
	private String message;
	
	
	public MessageEntity() {
		
	}

	public Integer getIdMessage() {
		return idMessage;
	}


	public void setIdMessage(Integer idMessage) {
		this.idMessage = idMessage;
	}


	public Integer getNamespace() {
		return namespace;
	}


	public void setNamespace(Integer namespace) {
		this.namespace = namespace;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	@Override
	public String toString() {
		return "MessageEntity [idMessage=" + idMessage + ", namespace=" + namespace + ", message=" + message + "]";
	}

}
