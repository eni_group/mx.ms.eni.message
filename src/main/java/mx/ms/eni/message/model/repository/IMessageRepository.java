package mx.ms.eni.message.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.ms.eni.message.model.entity.MessageEntity;

@Repository
public interface IMessageRepository extends JpaRepository<MessageEntity, Integer>{
	
	MessageEntity findByIdMessage(Integer idMessage);

}
