package mx.ms.eni.message.service;

import mx.ms.eni.message.model.dto.MessageDTO;

public interface IMessagePublicService {
	
	MessageDTO getMessage(Integer idMessage);
	
	void newMessage(MessageDTO messageDTO);
	
	void deleteMessage(Integer idMessage);
}
